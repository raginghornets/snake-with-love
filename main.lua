TILE_SIZE = 32

local grid = {
  width = 20,
  height = 20,
  color = {0.25, 0.75, 0.25, 1},
  draw = function (self)
    love.graphics.setColor(self.color[1], self.color[2], self.color[3], self.color[4])
    love.graphics.rectangle('fill', 0, 0, self.width * TILE_SIZE, self.height * TILE_SIZE)
  end,
  in_bounds = function (self, other)
    return other[1] >= 0 and other[1] < self.width and other[2] >= 0 and other[2] < self.height
  end,
}

local snake = {
  body = {{1, 9}, {2, 9}, {3, 9}},
  velocity = {0, 0},
  scale = {32, 32},
  color = {0, 0.5, 1, 1},
  draw = function (self)
    for i = 1, #self.body do
      love.graphics.setColor(self.color[1], self.color[2], self.color[3], self.color[4])
      love.graphics.rectangle('fill', self.body[i][1] * TILE_SIZE, self.body[i][2] * TILE_SIZE, self.scale[1], self.scale[2])
    end
  end,
  move = function (self)
    for i = 1, #self.body - 1 do
      self.body[i] = self.body[i + 1]
    end
    self.body[#self.body] = {self.body[#self.body][1] + self.velocity[1], self.body[#self.body][2] + self.velocity[2]}
  end,
  change_direction = function (self, key)
    if (key == 'left' or key == 'a') then
      self.velocity = {-1, 0}
    elseif (key == 'right' or key == 'd') then
      self.velocity = {1, 0}
    elseif (key == 'up' or key == 'w') then
      self.velocity = {0, -1}
    elseif (key == 'down' or key == 's') then
      self.velocity = {0, 1}
    end
  end,
  grow = function (self, apple)
    self.body[#self.body + 1] = {apple.position[1], apple.position[2]}
  end,
  self_collide = function (self)
    for x = 1, #self.body - 2 do
      if self.body[#self.body][1] == self.body[x][1] and self.body[#self.body][2] == self.body[x][2] then
        return true
      end
    end
    return false
  end,
  contains = function (self, apple)
    for x = 1, #self.body do
      if self.body[x][1] == apple.position[1] and self.body[x][2] == apple.position[2] then
        return true
      end
    end
    return false
  end,
  reset = function (self)
    self.body = {{1, 9}, {2, 9}, {3, 9}}
    self.velocity = {0, 0}
    love.timer.sleep(0.15)
  end,
  update = function (self, apple)
    if self.going_inwards(self) then
      self.go_outwards(self)
    end
    self.move(self)
    love.timer.sleep(0.075) -- Control how fast the snake is moving
  end,
  is_moving = function (self)
    return (self.velocity[1] ~= 0 or self.velocity[2] ~= 0)
  end,
  going_inwards = function (self)
    return self.body[#self.body][1] + self.velocity[1] == self.body[#self.body - 1][1] and self.body[#self.body][2] + self.velocity[2] == self.body[#self.body - 1][2]
  end,
  go_outwards = function (self)
    self.velocity = {self.velocity[1] * -1, self.velocity[2] * -1}
  end,
  collide = function (self, apple)
    return self.body[#self.body][1] == apple.position[1] and self.body[#self.body][2] == apple.position[2]
  end,
}

local apple = {
  position = {15, 9},
  scale = {32, 32},
  color = {1, 0, 0, 1},
  draw = function (self)
    love.graphics.setColor(self.color[1], self.color[2], self.color[3], self.color[4])
    love.graphics.rectangle('fill', self.position[1] * TILE_SIZE, self.position[2] * TILE_SIZE, self.scale[1], self.scale[2])
  end,
  reset = function (self, snake)
    while snake.contains(snake, self) do
      math.randomseed(os.time())
      self.position = {math.random(grid.width - 1), math.random(grid.height - 1)}
    end
  end
}

function love.load()
  love.window.setTitle('Snake')
  love.window.setMode(grid.width * TILE_SIZE, grid.height * TILE_SIZE)
end

function love.keypressed(key)
  if key == 'escape' then
    love.event.quit()
  end

  if key == 'space' then
    snake.velocity = {0, 0}
  end

  snake.change_direction(snake, key)
end

function love.update(dt)
  if grid.in_bounds(grid, snake.body[#snake.body]) and snake.is_moving(snake) and not snake.self_collide(snake) then
    if snake.collide(snake, apple) then
      snake.grow(snake, apple)
      apple.reset(apple, snake)
    else
      snake.update(snake, apple)
    end
  else
    snake.reset(snake)
  end
end

function love.draw()
  grid.draw(grid)
  apple.draw(apple)
  snake.draw(snake)
end